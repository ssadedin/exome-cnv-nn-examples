{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Toy example of training a LSTM to detect deletions\n",
    "\n",
    "This example creates simulation data mimicking deletion signals in coverage depth data\n",
    "and then trains an LSTM on that simulated data to recognise the deletions.\n",
    "\n",
    "Unlike the CNN example, we allow the CNVs to be variable size, between 10 and 50 bp long."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 87,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import torch.utils.data as datautils\n",
    "import copy\n",
    "from beakerx import *\n",
    "from typing import *\n",
    "from dataclasses import dataclass\n",
    "from sklearn.model_selection import ShuffleSplit\n",
    "import time\n",
    "import seaborn as sb\n",
    "\n",
    "%matplotlib widget\n",
    "beakerx.pandas_display_default()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Utility class to make it easier to simulate and test the deletions using range overlap calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.7142857142857143"
      ]
     },
     "execution_count": 88,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "@dataclass\n",
    "class Range():\n",
    "    \n",
    "    start : int\n",
    "    end : int\n",
    "        \n",
    "    def __len__(self):\n",
    "        return self.size()\n",
    "        \n",
    "    def size(self):\n",
    "        return self.end - self.start\n",
    "        \n",
    "    def subtract(self, other):\n",
    "        ix = self.intersect(other)\n",
    "        if not ix:\n",
    "            return copy.copy(self)\n",
    "        \n",
    "        result = []\n",
    "        if self.start < ix.start:\n",
    "            result.append(Range(self.start, ix.start))\n",
    "        if self.end > ix.end:\n",
    "            result.append(Range(ix.end, self.end))\n",
    "            \n",
    "        return result\n",
    "        \n",
    "    def frac_overlap(self, other):\n",
    "        \"\"\"\n",
    "        fraction of this range that has overlap with the other range\n",
    "        \"\"\"\n",
    "        ix = self.intersect(other)\n",
    "        if not ix:\n",
    "            return np.double(0)\n",
    "        return ix.size() / self.size()\n",
    "    \n",
    "    def mutual_overlap(self, other):\n",
    "        ix = self.intersect(other)\n",
    "        if not ix:\n",
    "            return 0\n",
    "        \n",
    "        ixSize = ix.size()\n",
    "        return min(ixSize / self.size(), ixSize / other.size())\n",
    "        \n",
    "    def intersect(self, other):\n",
    "        ixstart= max(self.start, other.start)\n",
    "        ixTo = min(self.end, other.end)\n",
    "        if ixTo<ixstart:\n",
    "            return None\n",
    "        return Range(ixstart,ixTo)\n",
    "\n",
    "\n",
    "r1 = Range(0,10)\n",
    "r2 = Range(5,12)\n",
    "\n",
    "r2.frac_overlap(r1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the purpose of the simulation, we make a Sample class representing a data set that either\n",
    "has or doesn't have a deletion. This tracks the start, end and actual data representing \n",
    "the sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Basic parameters for the simulation\n",
    "data_len = 220\n",
    "\n",
    "deletion_size_min = 10\n",
    "deletion_size_max = 50\n",
    "\n",
    "deletion_std_dev = 0.1\n",
    "num_positive_training_samples = 100\n",
    "\n",
    "num_controls = 100\n",
    "control_std_dev = 0.1\n",
    "\n",
    "low_std_dev = 0.10\n",
    "\n",
    "# A simple class to capture the data about a \"true positive\" training\n",
    "# sample\n",
    "@dataclass\n",
    "class Sample:\n",
    "  sample_id : str\n",
    "  covs : np.ndarray\n",
    "  sds : np.ndarray\n",
    "  contains_deletion : bool\n",
    "  deletion : Range\n",
    "    \n",
    "  @property\n",
    "  def deletionsize(self):\n",
    "        return self.deletion.size()\n",
    "    \n",
    "  def bin_state(self, bin : Range):\n",
    "    \n",
    "    if not self.deletion:\n",
    "        return 0\n",
    "   \n",
    "    frac_deleted = bin.frac_overlap(self.deletion)\n",
    "    \n",
    "    return 1 if frac_deleted > 0.5 else 0\n",
    "    \n",
    "  def get_copy_number_bins(self, num_bins : int):\n",
    "    bin_size = int(np.floor(data_len / num_bins))\n",
    "    cn = [\n",
    "        self.bin_state(Range(bin, bin+bin_size)) for bin in range(0,data_len,bin_size)\n",
    "    ]\n",
    "    return cn\n",
    "\n",
    "def create_positive_sample(sample_id : str):\n",
    "  \"\"\"\n",
    "  Creates a positive sample by selecting a random offset within \n",
    "  the data and creating a region of 0.5 coverage values within it\n",
    "  \"\"\"\n",
    "\n",
    "  deletion_size = int(np.random.uniform(deletion_size_min, deletion_size_max))\n",
    "\n",
    "  # select a random offset for the deletion\n",
    "  offset = int(np.random.uniform(deletion_size/2, data_len - deletion_size))\n",
    "  \n",
    "  values =  np.concatenate(\n",
    "      (np.random.normal(1, deletion_std_dev, offset), \n",
    "      np.random.normal(0.5, deletion_std_dev, deletion_size),\n",
    "      np.random.normal(1, deletion_std_dev, data_len-offset-deletion_size))\n",
    "  )\n",
    "    \n",
    "  sds = np.random.normal(low_std_dev, 0.03, data_len)\n",
    "  \n",
    "  return Sample(sample_id=sample_id, \n",
    "                covs=np.array(values, dtype=np.dtype('d')), \n",
    "                sds=np.array(sds, dtype=np.dtype('d')),\n",
    "                contains_deletion=True, \n",
    "                deletion=Range(offset,offset+deletion_size),\n",
    "               )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Create the training data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "positive_samples = [\n",
    "    create_positive_sample('positive_%s' % (sample_id+1))\n",
    "    for sample_id in range(0, num_positive_training_samples)\n",
    "]\n",
    "\n",
    "test_positive_samples = [\n",
    "    create_positive_sample('positive_%s' % (sample_id+1))\n",
    "    for sample_id in range(0, num_positive_training_samples)\n",
    "]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualisation of the Simulated Deletions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "0382a0502a9747b0aa6153230e11f244",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "for positive_sample in positive_samples[0:4]:\n",
    "  plt.plot(positive_sample.covs)\n",
    "\n",
    "plt.ylim((0,2))\n",
    "plt.title('Signal of positive training samples')\n",
    "plt.xlabel('Position in Simulated Chromosome')\n",
    "plt.ylabel('Value of Simulated Coverage Depth')\n",
    "None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Training Data for Controls\n",
    "\n",
    "Here by \"control\" we mean a sample that is negative ie: there are no deletions present in the data. We will label these \"0\" as a class that has no deletion to detect."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def create_control_sample(sample_id):\n",
    "  values = np.random.normal(1,control_std_dev,data_len)\n",
    "  sds = np.random.normal(low_std_dev,0.03,data_len)\n",
    "  return Sample(sample_id=sample_id, covs=values, contains_deletion=False, deletion=None, sds=sds)\n",
    "\n",
    "control_samples =  [create_control_sample(f\"control_${c}\") for c in range(0,num_controls)]\n",
    "\n",
    "test_control_samples =  [create_control_sample(f\"control_${c}\") for c in range(0,num_controls)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "14ddd34e838c48c9bf91ce275cb5722b",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "(0.0, 2.0)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "plt.close()\n",
    "\n",
    "for c in control_samples:\n",
    "  plt.plot(c.covs)\n",
    "\n",
    "plt.title('Coverage Values for Negative Training Samples')\n",
    "plt.xlabel('Position in Simulated Chromosome')\n",
    "plt.ylabel('Value of Simulated Coverage Depth')\n",
    "plt.ylim(0,2.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Noisy Controls with high Control Std Dev\n",
    "\n",
    "As an extension: we simulate a situation where the depth signal shows a depetion that looks like a deletion\n",
    "but there is also a high level of \"noise\", which should mean that the deletion is more likely just an aretfact of\n",
    "the noise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 89,
   "metadata": {},
   "outputs": [],
   "source": [
    "high_std_dev = 0.6\n",
    "\n",
    "\n",
    "def create_noisy_fp_sample(sample_id : str):\n",
    "  \"\"\"\n",
    "  Creates a sample where coverage dips but it coincides with a rise\n",
    "  in the std dev, suggesting a false positive\n",
    "  \"\"\"\n",
    "\n",
    "  deletion_size = int(np.random.uniform(deletion_size_min, deletion_size_max))\n",
    "\n",
    "  # select a random offset for the deletion\n",
    "  offset = int(np.random.uniform(deletion_size/2, data_len - deletion_size))\n",
    "  \n",
    "  values =  np.concatenate(\n",
    "      (np.random.normal(1, deletion_std_dev, offset), \n",
    "      np.random.normal(0.5, deletion_std_dev, deletion_size),\n",
    "      np.random.normal(1, deletion_std_dev, data_len-offset-deletion_size))\n",
    "  )\n",
    "    \n",
    "  sds =  np.concatenate(\n",
    "      (np.random.normal(low_std_dev, 0.03, offset), \n",
    "      np.random.normal(high_std_dev, 0.1, deletion_size),\n",
    "      np.random.normal(low_std_dev, 0.03, data_len-offset-deletion_size))\n",
    "  )\n",
    "  \n",
    "  return Sample(sample_id=sample_id, \n",
    "                covs=np.array(values, dtype=np.dtype('d')), \n",
    "                sds=np.array(sds, dtype=np.dtype('d')),\n",
    "                contains_deletion=False, \n",
    "                deletion=None\n",
    "               )\n",
    "\n",
    "noisy_fp_samples =  [create_noisy_fp_sample(f\"noisyfp_${c}\") for c in range(0,num_controls)]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "5d2a439dae2c464e85a577dfefd74f2a",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "(0.0, 2.0)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "plt.close()\n",
    "\n",
    "num_samples = 1\n",
    "\n",
    "for c in noisy_fp_samples[0:num_samples]:\n",
    "  plt.plot(c.covs, label='Normalised Coverage Depth')\n",
    "\n",
    "for c in noisy_fp_samples[0:num_samples]:\n",
    "  plt.plot(c.sds, label='Control Sample Standard Deviation')\n",
    "\n",
    "\n",
    "plt.title('Coverage Values for Negative Training Samples with Confounding Control Noise')\n",
    "plt.xlabel('Position in Simulated Chromosome')\n",
    "plt.ylabel('Value of Simulated Coverage Depth')\n",
    "plt.legend()\n",
    "plt.ylim(0,2.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialise the Global Test Results\n",
    "\n",
    "Make some global variables that track different configurations and their results.\n",
    "\n",
    "This is useful for tuning the hyper parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "all_test_results = {\n",
    "}\n",
    "\n",
    "nets = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create the LSTM"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_size = 20\n",
    "\n",
    "class CNVDataSet(datautils.Dataset):\n",
    "    \n",
    "    def __init__(self,samples):\n",
    "        self.samples = [\n",
    "            torch.tensor(list(zip(s.covs, s.sds)), dtype=torch.double) for s in samples\n",
    "        ]\n",
    "#         self.labels = [torch.tensor([0,1]).double() if s.deletion else torch.tensor([1,0]).double() for s in samples ]\n",
    "\n",
    "        # To train against a single output eg: as a pure classifier\n",
    "        self.labels = [torch.tensor(\n",
    "            any(x > 0.8 for x in s.get_copy_number_bins(20))\n",
    "        ).double() for s in samples]\n",
    "        \n",
    "        # To train to learn a sequence representing the copy number bins\n",
    "#         self.labels = [torch.tensor([x > 0.8 for x in s.get_copy_number_bins(20)], dtype=torch.double).double() for s in samples ]\n",
    "        \n",
    "    def __getitem__(self,index):\n",
    "        return (self.samples[index], self.labels[index])\n",
    "\n",
    "    def __len__(self):\n",
    "        return len(self.samples)\n",
    "    \n",
    "    def __repr__(self):\n",
    "        return f\"{len(self.samples)} samples containing {sum(self.labels)} true positives\"\n",
    "        \n",
    "\n",
    "# positive_dataset = d.TensorDataset(*[torch.tensor(ps.covs) for ps in positive_samples])\n",
    "# positive_dataset = datautils.TensorDataset(pos_data)\n",
    "\n",
    "positive_dataset = CNVDataSet(positive_samples)\n",
    "\n",
    "noisy_fp_dataset = CNVDataSet(noisy_fp_samples)\n",
    "\n",
    "test_positive_dataset = CNVDataSet(test_positive_samples)\n",
    "\n",
    "control_dataset = CNVDataSet(control_samples)\n",
    "\n",
    "test_control_dataset = CNVDataSet(test_control_samples)\n",
    "\n",
    "\n",
    "combined_dataset = CNVDataSet(positive_samples + control_samples + noisy_fp_samples) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create the LSTM Network\n",
    "\n",
    "Below we create the network itself.\n",
    "\n",
    "In this case we introduce a final fully connected layer that has only 1 output, and this forces\n",
    "the network to act like a pure classifier. Without that layer, it outputs a sequence that mirrors\n",
    "the input sequence but acts as a classifier for whether there's a deletion at each position along the\n",
    "sequence.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch.nn as nn\n",
    "import torch.nn.functional as F\n",
    "import torch.optim as optim\n",
    "\n",
    "criterion = nn.BCELoss()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 80,
   "metadata": {},
   "outputs": [],
   "source": [
    "lstm_hidden_size = 20\n",
    "lstm_layers = 1 \n",
    "output_size = 1\n",
    "drop_prob = 0\n",
    "\n",
    "class Net(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        self.hidden_dim = lstm_hidden_size\n",
    "        self.lstm = nn.LSTM(2, lstm_hidden_size, lstm_layers, dropout=0, batch_first=True, bidirectional=False)\n",
    "        self.dropout = nn.Dropout(drop_prob)\n",
    "        self.fc = nn.Linear(lstm_hidden_size, 1)\n",
    "        self.fc2 = nn.Linear(data_len, output_size)\n",
    "        self.sigmoid = nn.Sigmoid()\n",
    "\n",
    "    def forward(self, x):\n",
    "        lstm_out, hidden = self.lstm(x)\n",
    "        out = self.dropout(lstm_out)\n",
    "        out = self.fc(out)\n",
    "        out = self.fc2(out.reshape(data_len))        \n",
    "        out = self.sigmoid(out)\n",
    "        return out\n",
    "    \n",
    "    def predict(self, data,index):\n",
    "\n",
    "        test_data = data[index][0].view(-1,1,data[index][0].size()[1])\n",
    "\n",
    "        return list(self(test_data).reshape(1).detach().numpy())[0]\n",
    "    \n",
    "    def predict_all(self, data,index):\n",
    "\n",
    "        test_data = data[index][0].view(-1,1,data[index][0].size()[1])\n",
    "\n",
    "        return list(self(test_data).reshape(1).detach().numpy())\n",
    "\n",
    "    \n",
    "#     def init_hidden(self, batch_size):\n",
    "#             weight = next(self.parameters()).data\n",
    "#             hidden = (weight.new(self.n_layers, batch_size, self.hidden_dim).zero_().to(device),\n",
    "#                       weight.new(self.n_layers, batch_size, self.hidden_dim).zero_().to(device))\n",
    "#             return hidden    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "metadata": {},
   "outputs": [],
   "source": [
    "net = Net().double()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a training function\n",
    "\n",
    "We need function that does the work of running inputs through the network\n",
    "and then calculating the loss and adjusting weights baesd on the loss.\n",
    "\n",
    "To set it up to do cross validation easily, the function takes the indexes of the true positive and negative\n",
    "control samples to use in training.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "metadata": {},
   "outputs": [],
   "source": [
    "tps = positive_samples\n",
    "fps = control_samples\n",
    "\n",
    "num_epochs = 1000\n",
    "torch.set_num_threads=8\n",
    "learning_rate = 0.001\n",
    "running_loss_threshold = 0.1\n",
    "\n",
    "def train(tp_train_indexes=None, fp_train_indexes=None):\n",
    "    \n",
    "    if tp_train_indexes is not None:\n",
    "        train_data = [tps[i] for i in tp_train_indexes] + [fps[i] for i in fp_train_indexes] \n",
    "        combined_dataset = CNVDataSet(train_data)\n",
    "    else:\n",
    "        combined_dataset = CNVDataSet(tps + fps)\n",
    "    \n",
    "    training_loader = datautils.DataLoader(combined_dataset, batch_size=1, shuffle=True, num_workers=0)\n",
    "    \n",
    "    criterion = nn.BCELoss()\n",
    "\n",
    "    net = Net().double()\n",
    "\n",
    "    optimizer = optim.SGD(net.parameters(), lr=learning_rate, momentum=0.9)\n",
    "\n",
    "    data = None\n",
    "\n",
    "    for epoch in range(num_epochs):\n",
    "\n",
    "        running_loss = 0.0\n",
    "\n",
    "        for i, data in enumerate(training_loader, 0):\n",
    "                optimizer.zero_grad()\n",
    "\n",
    "                inputs,labels = data\n",
    "\n",
    "                outputs = net(inputs)\n",
    "\n",
    "                loss = criterion(outputs, labels)\n",
    "\n",
    "                loss.backward()\n",
    "\n",
    "                running_loss += loss.item()\n",
    "\n",
    "                optimizer.step()\n",
    "\n",
    "        if epoch % 10 == 0:\n",
    "            print(\"%s\\tEpoch %d - Total loss = %.3f\" % (time.asctime(), epoch, running_loss))\n",
    "        \n",
    "        if running_loss < running_loss_threshold:\n",
    "            break\n",
    "\n",
    "    print(f\"Done: Loss = {running_loss}\") \n",
    "    \n",
    "    return net"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set up cross validation\n",
    "\n",
    "To make cross validation easier, create a class that captures the indices used for any given\n",
    "split of the training data along with the results from training and testing the split.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 84,
   "metadata": {},
   "outputs": [],
   "source": [
    "@dataclass\n",
    "class TestSplit:\n",
    "    \n",
    "    # ===============    input parameters   ======================\n",
    "    tag : str\n",
    "    index : int\n",
    "    tp_dataset : CNVDataSet\n",
    "    fp_dataset : CNVDataSet\n",
    "    tp_train_indexes : list\n",
    "    tp_test_indexes : list\n",
    "    fp_train_indexes : list\n",
    "    fp_test_indexes : list\n",
    "    \n",
    "    # ================ populated by methods ====================\n",
    "    \n",
    "    net : Net = None\n",
    "    tp_test_results : pd.DataFrame = None\n",
    "    fp_test_results : pd.DataFrame = None\n",
    "    tp_mean : np.double = None\n",
    "    tp_sd : np.double = None\n",
    "    fp_mean : np.double = None\n",
    "    fp_sd : np.double = None\n",
    "                \n",
    "    def test(self):\n",
    "\n",
    "        self.tp_test_results = pd.DataFrame({'split':[i]*len(self.tp_test_indexes), \n",
    "                                    'indices': self.tp_test_indexes,\n",
    "                                    'expected': [1.0]*len(self.tp_test_indexes),\n",
    "                                    'actual':[self.net.predict(self.tp_dataset,i) for i in self.tp_test_indexes ]\n",
    "                                   })\n",
    "        self.fp_test_results = pd.DataFrame({'split':[i]*len(self.fp_test_indexes), \n",
    "                                    'indices': self.fp_test_indexes,\n",
    "                                    'expected': [0.0]*len(self.fp_test_indexes),\n",
    "                                    'actual':[self.net.predict(self.fp_dataset,i) for i in self.fp_test_indexes ]\n",
    "                                   })\n",
    "  \n",
    "    def train(self):\n",
    "       \n",
    "        self.net = train(self.tp_train_indexes, self.fp_train_indexes)\n",
    "        \n",
    "        tp_preds = [self.net.predict(self.tp_dataset,i) for i in range(len(self.tp_dataset))] \n",
    "        \n",
    "        self.tp_mean = np.mean(tp_preds)\n",
    "        self.tp_sd = np.std(tp_preds)\n",
    "        \n",
    "        fp_preds = [self.net.predict(self.fp_dataset,i) for i in range(len(self.fp_dataset))] \n",
    "        \n",
    "        self.fp_mean = np.mean(fp_preds)\n",
    "        self.fp_sd = np.std(fp_preds)\n",
    "         \n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train with Cross Validation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 100,
   "metadata": {},
   "outputs": [],
   "source": [
    "tp_dataset=CNVDataSet(tps)\n",
    "fp_dataset=CNVDataSet(fps)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 106,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Sun Nov 22 17:34:35 2020\tEpoch 0 - Total loss = 111.114\n",
      "Sun Nov 22 17:35:29 2020\tEpoch 10 - Total loss = 110.994\n",
      "Sun Nov 22 17:36:23 2020\tEpoch 20 - Total loss = 110.913\n",
      "Sun Nov 22 17:37:17 2020\tEpoch 30 - Total loss = 109.484\n",
      "Sun Nov 22 17:38:12 2020\tEpoch 40 - Total loss = 92.884\n",
      "Sun Nov 22 17:39:06 2020\tEpoch 50 - Total loss = 101.918\n",
      "Sun Nov 22 17:40:01 2020\tEpoch 60 - Total loss = 66.791\n",
      "Sun Nov 22 17:40:55 2020\tEpoch 70 - Total loss = 40.631\n",
      "Sun Nov 22 17:41:49 2020\tEpoch 80 - Total loss = 43.786\n",
      "Sun Nov 22 17:42:43 2020\tEpoch 90 - Total loss = 14.885\n",
      "Sun Nov 22 17:43:38 2020\tEpoch 100 - Total loss = 4.213\n",
      "Sun Nov 22 17:44:32 2020\tEpoch 110 - Total loss = 0.534\n",
      "Sun Nov 22 17:45:26 2020\tEpoch 120 - Total loss = 0.232\n",
      "Sun Nov 22 17:46:20 2020\tEpoch 130 - Total loss = 0.141\n",
      "Sun Nov 22 17:47:14 2020\tEpoch 140 - Total loss = 0.109\n",
      "Done: Loss = 0.0957822184188639\n",
      "Sun Nov 22 17:47:31 2020\tEpoch 0 - Total loss = 111.289\n",
      "Sun Nov 22 17:48:25 2020\tEpoch 10 - Total loss = 111.039\n",
      "Sun Nov 22 17:49:19 2020\tEpoch 20 - Total loss = 109.587\n",
      "Sun Nov 22 17:50:13 2020\tEpoch 30 - Total loss = 102.348\n",
      "Sun Nov 22 17:51:07 2020\tEpoch 40 - Total loss = 77.458\n",
      "Sun Nov 22 17:52:01 2020\tEpoch 50 - Total loss = 93.101\n",
      "Sun Nov 22 17:52:55 2020\tEpoch 60 - Total loss = 71.478\n",
      "Sun Nov 22 17:53:49 2020\tEpoch 70 - Total loss = 50.915\n",
      "Sun Nov 22 17:54:43 2020\tEpoch 80 - Total loss = 23.092\n",
      "Sun Nov 22 17:55:37 2020\tEpoch 90 - Total loss = 11.947\n",
      "Sun Nov 22 17:56:32 2020\tEpoch 100 - Total loss = 4.506\n",
      "Sun Nov 22 17:57:26 2020\tEpoch 110 - Total loss = 1.663\n",
      "Sun Nov 22 17:58:20 2020\tEpoch 120 - Total loss = 0.768\n",
      "Sun Nov 22 17:59:15 2020\tEpoch 130 - Total loss = 0.374\n",
      "Sun Nov 22 18:00:09 2020\tEpoch 140 - Total loss = 0.242\n",
      "Sun Nov 22 18:01:04 2020\tEpoch 150 - Total loss = 0.186\n",
      "Sun Nov 22 18:01:58 2020\tEpoch 160 - Total loss = 0.112\n",
      "Done: Loss = 0.09844083566352886\n",
      "====== Finished ======\n"
     ]
    }
   ],
   "source": [
    "num_splits = 2\n",
    "\n",
    "tp_splitter = ShuffleSplit(n_splits=num_splits, test_size=0.2)\n",
    "fp_splitter = ShuffleSplit(n_splits=num_splits, test_size=0.2)\n",
    "\n",
    "test_results = pd.DataFrame({'split':[], 'indices':[], 'expected':[], 'actual':[]})\n",
    "\n",
    "test_tag = f'hidden{lstm_hidden_size}_stop{running_loss_threshold}_dp{drop_prob}_{lstm_layers}h' \n",
    "\n",
    "test_nets = nets.setdefault(test_tag, [])\n",
    "\n",
    "splits = []\n",
    "\n",
    "\n",
    "for i in range(num_splits):\n",
    "    \n",
    "    tp_train, tp_test = next(tp_splitter.split(tps))\n",
    "    fp_train, fp_test = next(fp_splitter.split(fps))\n",
    "    \n",
    "    split = TestSplit(tag=test_tag, index=i,\n",
    "                      tp_dataset=CNVDataSet(tps),\n",
    "                      fp_dataset=CNVDataSet(fps),\n",
    "                      tp_train_indexes=tp_train, tp_test_indexes=tp_test,\n",
    "                      fp_train_indexes=fp_train, fp_test_indexes=fp_test,\n",
    "                     )\n",
    "    \n",
    "    split.train()\n",
    "    \n",
    "    split.test()\n",
    "    \n",
    "    splits.append(split)\n",
    "    \n",
    "    test_results = pd.concat([test_results, split.tp_test_results, split.fp_test_results])                                     \n",
    " \n",
    "\n",
    "print('====== Finished ======')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 107,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "5cad3ff654bd46d18fd2a59c907937e5",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "def plot_split(split):\n",
    "    plt.close()\n",
    "    ax = sb.kdeplot(x=split.tp_test_results.actual, bw_method=0.2, shade=True, label='Positives')\n",
    "    ax = sb.kdeplot(x=split.fp_test_results.actual, bw_method=0.2, shade=True, label='Controls')\n",
    "    ax.set_title(split.tag)\n",
    "    ax.set_xlabel('Score')\n",
    "    ax.axvline(splits[0].tp_mean, color='blue')\n",
    "    ax.axvline(splits[0].fp_mean, color='orange')\n",
    "    ax.legend()\n",
    "    return ax\n",
    "    \n",
    "    \n",
    "# splits[0].train_mean\n",
    "\n",
    "ax = plot_split(splits[0])\n",
    "\n",
    "# splits[0].fp_test_results\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 108,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4.92448402453886e-05"
      ]
     },
     "execution_count": 108,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.std([net.predict(tp_dataset,i) for i in range(len(tp_dataset))])   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 109,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>split</th>\n",
       "      <th>indices</th>\n",
       "      <th>expected</th>\n",
       "      <th>actual</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.0</td>\n",
       "      <td>86.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.999611</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.0</td>\n",
       "      <td>6.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.946595</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.0</td>\n",
       "      <td>58.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.987275</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0.0</td>\n",
       "      <td>93.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.999436</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.0</td>\n",
       "      <td>2.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>0.677496</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15</th>\n",
       "      <td>1.0</td>\n",
       "      <td>72.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.999966</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>1.0</td>\n",
       "      <td>60.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.999992</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17</th>\n",
       "      <td>1.0</td>\n",
       "      <td>64.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.999979</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>18</th>\n",
       "      <td>1.0</td>\n",
       "      <td>58.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.999960</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>1.0</td>\n",
       "      <td>41.0</td>\n",
       "      <td>0.0</td>\n",
       "      <td>0.999924</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>80 rows × 4 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "    split  indices  expected    actual\n",
       "0     0.0     86.0       1.0  0.999611\n",
       "1     0.0      6.0       1.0  0.946595\n",
       "2     0.0     58.0       1.0  0.987275\n",
       "3     0.0     93.0       1.0  0.999436\n",
       "4     0.0      2.0       1.0  0.677496\n",
       "..    ...      ...       ...       ...\n",
       "15    1.0     72.0       0.0  0.999966\n",
       "16    1.0     60.0       0.0  0.999992\n",
       "17    1.0     64.0       0.0  0.999979\n",
       "18    1.0     58.0       0.0  0.999960\n",
       "19    1.0     41.0       0.0  0.999924\n",
       "\n",
       "[80 rows x 4 columns]"
      ]
     },
     "execution_count": 109,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "test_results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 104,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_xv(tag, splits=None):\n",
    "    tr = all_test_results.setdefault(tag,test_results)\n",
    "    plt.close()\n",
    "    if not splits:\n",
    "        splits = set([0.0,1.0,2.0,3.0,4.0])\n",
    "    sb.kdeplot(x=tr.actual[(tr.expected<0.01) & (tr.split.isin(splits))], bw_method=0.2, shade=True, label='Controls')\n",
    "    ax = sb.kdeplot(x=tr.actual[(tr.expected>0.8) & (tr.split.isin(splits))], bw_method=0.2, shade=True, label='Positives')\n",
    "    ax.set_title(tag)\n",
    "    ax.set_xlabel('Score')\n",
    "    ax.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 111,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "f9314daa435e4debadf3510b4d8dea3f",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plot_xv(test_tag)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
